//
//  MainTableViewController.swift
//  iOSExerciseSwift
//
//  Created by Daniel D'Abate on 3/8/17.
//  Copyright © 2017 Daniel D'Abate. All rights reserved.
//

import UIKit

private extension ButtonType {
    
    static var allNibNames = [ButtonType.left : "ButtonLeftTableViewCell",
                              ButtonType.right : "ButtonRightTableViewCell",
                              ButtonType.top : "ButtonTopTableViewCell"]
    
    func nibName() -> String {
        return ButtonType.allNibNames[self] ?? ""
    }
}

class MainTableViewController : UITableViewController, ButtonTableViewCellDelegate {
    
    var buttonModels = [] as [ButtonModel]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Register cell nibs
        for (_, nibName) in ButtonType.allNibNames {
            tableView.register(UINib(nibName: nibName, bundle: nil), forCellReuseIdentifier: nibName)
        }
        
        // Setup table view
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 70
        
        // Load data from backend
        InterfaceService.sharedInstance.getButtons(callback: { (newButtonModels) in
            
            if let newButtonModels = newButtonModels {
                buttonModels = newButtonModels
                tableView.reloadData()
            }
        })
    }
    
    // MARK: - UITableViewDatasource
    // Return qty of buttonModels as sections and not rows to be able to show spaces between them with section footers
    override func numberOfSections(in tableView: UITableView) -> Int {
        return buttonModels.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let buttonModel = buttonModels[indexPath.section]
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: buttonModel.type.nibName()) as? ButtonTableViewCell
            else { return UITableViewCell() }
        
        // Cell setup
        cell.titleLabel.text = buttonModel.name
        cell.delegate = self
        
        return cell
    }
    
    // MARK: - UITableViewDelegate
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return (section == 0 ? 22 : 0)
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 22
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView()
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    // MARK: - ButtonTableViewCellDelegate
    func didSelect(cell: ButtonTableViewCell) {

        if let indexPath = tableView.indexPath(for: cell) {
            let buttonModel = buttonModels[indexPath.section]
            
            let alertController = UIAlertController(title: buttonModel.name, message: "", preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "Close", style: .default, handler: nil))
            
            present(alertController, animated: true, completion: nil)
        }
    }
}
