//
//  InterfaceService.swift
//  iOSExerciseSwift
//
//  Created by Daniel D'Abate on 3/8/17.
//  Copyright © 2017 Daniel D'Abate. All rights reserved.
//

import UIKit

class InterfaceService {
    
    static let sharedInstance = InterfaceService()
    
    static private let buttonsKey = "buttons"
    
    func getButtons(callback: (_ buttons: [ButtonModel]?) -> Void) {
    
        // Get data from interface service
        guard let interfaceServiceDataFilePath = Bundle.main.path(forResource: "InterfaceServiceData", ofType: "json")
        else {
            callback(nil)
            return
        }
        
        guard let interfaceServiceData = NSData(contentsOfFile: interfaceServiceDataFilePath)
        else {
            callback(nil)
            return
        }
        
        // Get button models from interface service data
        do {
            let interfaceElementsDictionary = try JSONSerialization.jsonObject(with: interfaceServiceData as Data, options: .allowFragments) as? Dictionary<String, Any>
            guard let buttonsArray = interfaceElementsDictionary?[InterfaceService.buttonsKey] as? [Dictionary<String, Any>]
            else {
                callback(nil)
                return
            }
            
            let buttonModels = ButtonModel.buttonModels(from: buttonsArray)
            callback(buttonModels)

        } catch {
            callback(nil)
            return
        }
    }
}
