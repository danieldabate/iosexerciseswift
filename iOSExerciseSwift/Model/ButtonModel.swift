//
//  ButtonModel.swift
//  iOSExerciseSwift
//
//  Created by Daniel D'Abate on 3/8/17.
//  Copyright © 2017 Daniel D'Abate. All rights reserved.
//

import UIKit

enum ButtonType : String {
    case left = "left"
    case right = "right"
    case top = "top"
}

class ButtonModel {
    
    let type:ButtonType
    let name:String
    
    init(type:ButtonType, name:String) {
        self.type = type
        self.name = name
    }
}

// MARK: - Factory Methods
extension ButtonModel {
    
    static private let typeKey = "type"
    static private let nameKey = "name"

    class func buttonModel(from dictionary:Dictionary<String, Any>) -> ButtonModel? {
        
        guard let type = ButtonType(rawValue: dictionary[typeKey] as? String ?? "")
            else { return nil }
        
        guard let name = dictionary[nameKey] as? String
            else { return nil }
        
        return ButtonModel(type: type, name: name)
    }
    
    class func buttonModels(from array:[Dictionary<String, Any>]) -> [ButtonModel] {
        
        var buttonsArray = [] as [ButtonModel]
        
        for buttonDictionary in array {
            
            if let buttonModel = buttonModel(from: buttonDictionary) {
                buttonsArray.append(buttonModel)
            }
        }
        
        return buttonsArray
    }
}
