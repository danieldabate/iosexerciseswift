//
//  ButtonTableViewCell.swift
//  iOSExerciseSwift
//
//  Created by Daniel D'Abate on 3/8/17.
//  Copyright © 2017 Daniel D'Abate. All rights reserved.
//

import UIKit

protocol ButtonTableViewCellDelegate : NSObjectProtocol {
    
    func didSelect(cell:ButtonTableViewCell)
}

class ButtonTableViewCell : UITableViewCell {
    
    weak var delegate:ButtonTableViewCellDelegate?

    @IBOutlet weak private var actionButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Button visual setup
        actionButton.backgroundColor = UIColor(red: 0.0, green: 122.0/255.0, blue: 1.0, alpha: 1.0)
        actionButton.setTitleColor(UIColor.white, for: .normal)
        
        actionButton.contentEdgeInsets = UIEdgeInsetsMake(7.0, 15.0, 7.0, 15.0)
        actionButton.layer.cornerRadius = 8.0
    }
    
    @IBAction func actionButtonPressed() {
        delegate?.didSelect(cell: self)
    }
}
